# **Mobile Coding Challenge for HIDDEN FOUNDERS**

**Description:** Thechnical Test as a small app that will list the most starred Github repos that were created in the last 30 days. We'll be fetching the sorted JSON data directly from the Github API. 

**Google play link:** https://play.google.com/store/apps/details?id=hidden_founders.lamrani.com.hiddenfounderscodingchallenge

**Developer:** Lamrani Othmane (lamraniothmane1@gmail.com).

**Source code:** https://gitlab.com/lamraniothmane1/hidden-founders-challenge

**Technologies used:** JAVA, XML.

# *1.	User stories :*

The user access the application without any registration system. Once opened, a splash screen is shown to use, it is containing the logo of Hidden Founders, Git logo and the name of the developer.
 
The application provides the following features:

-	**As a User I should be able to list the most starred Github repos that were created in the last 30 days.**

-	**As a User I should be able to see for each repo/row the following details :**

    -	Repository name;
    -	Repository description;
    -   Numbers of stars for the repo;
    -   Username and avatar of the owner.
    
-	**As a User I should be able to keep scrolling and new results should appear (pagination).**

-	**Once an item of the list is clicked we can view more details about that repository:** 

    -	Repository full name;
    -	Last update of the repository;
    -   Numbers of stars for the repo;
    -   Username and avatar of the owner;
    -   Repository Licence;
    -   Clickable Repository Owner; 
    -   Clickable Repository URL; 
    

# *3.	External libraries:*

This project uses the following external libraries
-   **Retrofit 2.5**
    -	Library Url: http://square.github.io/picasso/
    -   Retrofit is a REST Client for Java and Android. It makes it relatively easy to retrieve and upload JSON (or other structured data) via a REST based webservice.
    -   API URL = https://api.github.com/search/repositories?q=created:>2017-10-22&sort=stars&order=desc&page=2

-   **Picasso**
    -	Library Url: http://square.github.io/retrofit/
    -   Picasso allows for hassle-free image loading in one line of code, and we can also load images using their URL;
   
-   **hdodenhof/CircleImageView**
    -	Library Url: https://github.com/hdodenhof/CircleImageView
    -   A fast circular ImageView perfect for profile round images.