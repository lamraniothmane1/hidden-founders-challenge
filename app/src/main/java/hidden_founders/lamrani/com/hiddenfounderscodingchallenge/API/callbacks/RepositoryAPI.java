package hidden_founders.lamrani.com.hiddenfounderscodingchallenge.API.callbacks;

import java.util.ArrayList;
import java.util.List;
import hidden_founders.lamrani.com.hiddenfounderscodingchallenge.API.models.RepositoryResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Lamrani on 08/01/2019.
 */

public interface RepositoryAPI {

    /**
     * Get all the repositories from Git hub apiAPI
     * @return a list of repositories
     */
    @GET("search/repositories?sort=stars&order=desc")
    Call<RepositoryResponse> getRepositories(@Query("q") String date, @Query("page") int page);




}
