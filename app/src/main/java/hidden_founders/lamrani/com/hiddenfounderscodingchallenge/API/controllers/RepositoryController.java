package hidden_founders.lamrani.com.hiddenfounderscodingchallenge.API.controllers;

import java.util.ArrayList;
import java.util.List;

import hidden_founders.lamrani.com.hiddenfounderscodingchallenge.API.OperatingApiClient;
import hidden_founders.lamrani.com.hiddenfounderscodingchallenge.API.callbacks.RepositoryAPI;
import hidden_founders.lamrani.com.hiddenfounderscodingchallenge.API.models.Repository;
import hidden_founders.lamrani.com.hiddenfounderscodingchallenge.API.models.RepositoryResponse;
import retrofit2.Call;
import retrofit2.Retrofit;

/**
 * Created by Lamrani on 08/01/2019.
 */

public class RepositoryController {

    private RepositoryAPI repositoryAPI;

    public RepositoryController(){
        Retrofit retrofit = OperatingApiClient.getClient();
        repositoryAPI =  retrofit.create(RepositoryAPI.class);
    }

    /**
     * Get all the repositories from Git hub apiAPI
     * @param date (30 days before)
     * @param page (pagination number)
     * @return
     */
    public Call<RepositoryResponse> getRepositories(String date, int page){
        date = "created:>" + date;
        return repositoryAPI.getRepositories(date, page);
    }

}
