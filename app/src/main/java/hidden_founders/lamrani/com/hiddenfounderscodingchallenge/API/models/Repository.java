package hidden_founders.lamrani.com.hiddenfounderscodingchallenge.API.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.ParseException;

import hidden_founders.lamrani.com.hiddenfounderscodingchallenge.Helper.Utility;

/**
 * Created by Lamrani on 08/01/2019.
 */

public class Repository implements Serializable{

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("full_name")
    private String full_name;

    @Expose
    @SerializedName("html_url")
    private String html_url;

    @Expose
    @SerializedName("description")
    private String description;

    @Expose
    @SerializedName("updated_at")
    private String updated_at;

    @Expose
    @SerializedName("stargazers_count")
    private int stargazers_count;

    @Expose
    @SerializedName("owner")
    private Owner owner;

    @Expose
    @SerializedName("license")
    private Licence license;

    public String getName() {
        return name;
    }

    public String getFull_name() {
        return full_name;
    }

    public String getHtml_url() {
        return html_url;
    }

    public String getDescription() {
        return description;
    }

    public String getUpdated_at() {
        try {
            String date = Utility.getDate(updated_at);
            if(date != null){
                return date;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return updated_at;
    }


    public String getStargazers_count() {

        if(stargazers_count < 1000){
            return String.valueOf(stargazers_count);
        }
        float result = (float) stargazers_count/1000;
        return new DecimalFormat("##.#").format(result) + " K";
    }

    public Owner getOwner() {
        return owner;
    }

    public Licence getLicense() {
        return license;
    }

    public class Licence implements Serializable{
        @Expose
        @SerializedName("name")
        String name;

        public String getName() {
            return name;
        }
    }


    public class Owner implements Serializable{

        @Expose
        @SerializedName("login")
        String login;

        @Expose
        @SerializedName("avatar_url")
        String avatar_url;

        @Expose
        @SerializedName("html_url")
        String html_url;

        public String getLogin() {
            return login;
        }

        public String getAvatar_url() {
            return avatar_url;
        }

        public String getHtml_url() {
            return html_url;
        }
    }


}
