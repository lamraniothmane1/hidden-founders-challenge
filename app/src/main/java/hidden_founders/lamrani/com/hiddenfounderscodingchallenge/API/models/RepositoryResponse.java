package hidden_founders.lamrani.com.hiddenfounderscodingchallenge.API.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lamrani on 08/01/2019.
 */

public class RepositoryResponse {

    @Expose
    @SerializedName("items")
    private ArrayList<Repository> items;

    public ArrayList<Repository> getItems() {
        return items;
    }
}
