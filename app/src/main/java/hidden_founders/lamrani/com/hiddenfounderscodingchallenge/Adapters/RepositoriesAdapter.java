package hidden_founders.lamrani.com.hiddenfounderscodingchallenge.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import hidden_founders.lamrani.com.hiddenfounderscodingchallenge.API.models.Repository;
import hidden_founders.lamrani.com.hiddenfounderscodingchallenge.Helper.Utility;
import hidden_founders.lamrani.com.hiddenfounderscodingchallenge.R;
import hidden_founders.lamrani.com.hiddenfounderscodingchallenge.UI.MainActivity;
import hidden_founders.lamrani.com.hiddenfounderscodingchallenge.UI.fragment.FragmentDetails;

import static hidden_founders.lamrani.com.hiddenfounderscodingchallenge.Helper.Constants.REPOSITORY;

/**
 * Created by Lamrani on 08/01/2019.
 */

public class RepositoriesAdapter extends RecyclerView.Adapter<RepositoriesAdapter.Holder> {

    private List<Repository> items_data;
    private MainActivity activity;



    public RepositoriesAdapter(MainActivity activity) {
        items_data = new ArrayList<>();
        this.activity = activity;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_repository, parent, false);
        return new Holder(row);
    }


    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {

        holder.itemView.setLongClickable(true);

        Repository repository = getItem(position);

        if(repository != null){
            // get values
            String title = repository.getName();
            String subtitle = repository.getDescription();

            Repository.Owner owner = repository.getOwner();
            String owner_login = owner != null ? owner.getLogin() : activity.getString(R.string.unavailable);

            String rating = repository.getStargazers_count();
            String avatar_url = (repository.getOwner()).getAvatar_url();

            // set values
            holder.tv_title.setText(title);
            holder.tv_subtitle.setText(subtitle);
            holder.tv_user.setText(owner_login);
            holder.tv_rating.setText(rating);
            // set image with picasso
            if(avatar_url != null){
                if(!avatar_url.isEmpty()){
                    Picasso.get().load(avatar_url)
                            .fit()
                            .error(R.drawable.ic_user)
                            .into(holder.iv_avatar);
                }
            }
        }

    }

    @Override
    public int getItemCount() {
        return items_data.size();
    }

    /**
     * Add an item to the list
     * @param item
     */
    public void addItem(Repository item){
        items_data.add(item);
        notifyDataSetChanged();
    }

    /**
     * Add a list to the items
     * @param repositories
     */
    public void addAll(List<Repository> repositories){
        items_data.addAll(repositories);
        notifyDataSetChanged();
    }

    /**
     * Clear the list
     */
    public void removeAllItems(){
        items_data.clear();
        notifyDataSetChanged();
    }

    public void removeItem(int position){
        items_data.remove(position);
        notifyDataSetChanged();
    }

    /**
     * get an item at position
     */
    public Repository getItem(int position){
        return items_data.get(position);
    }


    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private Context context;
        private TextView tv_title, tv_subtitle, tv_user, tv_rating ;
        ImageView iv_avatar;

        Holder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            context = itemView.getContext();

            tv_title = itemView.findViewById(R.id.tv_title);
            tv_subtitle = itemView.findViewById(R.id.tv_subtitle);
            tv_user = itemView.findViewById(R.id.tv_user);
            tv_rating = itemView.findViewById(R.id.tv_rating);

            iv_avatar = itemView.findViewById(R.id.iv_avatar);
        }

        @Override
        public void onClick(View view) {

            if(!Utility.isNetworkAvailable(context)){
                Toast.makeText(context, R.string.network_unavailable, Toast.LENGTH_SHORT).show();
            }
            else{
                Repository selected_item = getItem(getAdapterPosition());

                FragmentDetails fragmentDetails = new FragmentDetails();
                Bundle bundle = new Bundle();
                bundle.putSerializable(REPOSITORY, selected_item);
                fragmentDetails.setArguments(bundle);
                activity.showFragment(fragmentDetails);

            }



        }


    }

}
