package hidden_founders.lamrani.com.hiddenfounderscodingchallenge.Helper;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import hidden_founders.lamrani.com.hiddenfounderscodingchallenge.R;


/**
 * Created by Lamrani on 08/01/2019.
 */

public class Utility {


    /**
     * Check if the network is available
     * @param context
     * @return
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    /**
     *
     * @param context
     * @return the current day in that format 08/01/2019
     */
    public static String getDateBeforeOneMonth(Context context){
        Date today = new Date();
        Calendar cal = new GregorianCalendar();
        cal.setTime(today);
        cal.add(Calendar.DAY_OF_MONTH, -30);
        Date date_before_one_month = cal.getTime();
        return new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(date_before_one_month);
    }

    public static String getDate(String string_date) throws ParseException {
        String result = "";
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);
        Date date = df.parse(string_date);
        DateFormat df2 = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);


        return df2.format(date);
    }

    /**
     * Set current language
     */
    // Change locale application language
    public static void changeLocaleLanguage(Activity activity, String lang){
        final ProgressDialog progressDialog = new ProgressDialog(activity);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(activity.getString(R.string.loading));
        progressDialog.show();

        Locale myLocale = new Locale(lang);
        Resources res = activity.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);

        progressDialog.dismiss();

        //activity.showFragment(new SettingsFragment());

    }

    /**
     * set local langauage
     */
    public static void setLocale(Context context){
        // Shared preference
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String language_value = sharedPreferences.getString("language", "en");
        Locale locale = new Locale(language_value);
        Locale.setDefault(locale);
        Configuration config = context.getResources().getConfiguration();
        config.locale= locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
    }

}
