package hidden_founders.lamrani.com.hiddenfounderscodingchallenge.UI;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;
import java.util.List;
import hidden_founders.lamrani.com.hiddenfounderscodingchallenge.API.controllers.RepositoryController;
import hidden_founders.lamrani.com.hiddenfounderscodingchallenge.API.models.Repository;
import hidden_founders.lamrani.com.hiddenfounderscodingchallenge.API.models.RepositoryResponse;
import hidden_founders.lamrani.com.hiddenfounderscodingchallenge.Adapters.RepositoriesAdapter;
import hidden_founders.lamrani.com.hiddenfounderscodingchallenge.Helper.EndlessRecyclerViewScrollListener;
import hidden_founders.lamrani.com.hiddenfounderscodingchallenge.Helper.Utility;
import hidden_founders.lamrani.com.hiddenfounderscodingchallenge.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener{

    private static final String TAG = "MainActivity";

    // Recycler view & adapter for repositories
    RecyclerView recyclerView;
    RepositoriesAdapter repositoriesAdapter;

    // for swipe down refresh
    private SwipeRefreshLayout swipeRefreshLayout;

    // Endless recycler view pagination loader
    EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;

    // BottomNavigationView
    private BottomNavigationView navigation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // initialize the view
        initView();

        // load repositories
        loadRepositories(1);
    }

    /**
     * Initialize UI components
     */
    private void initView() {
        // Bottom navigation view
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(this);

        // config recycler view
        configRecyclerView();

        // configure the swipe refresh layout
        configSwipeRefreshLayout();

    }

    /**
     * Set up the swipe refresh layout
     */
    private void configSwipeRefreshLayout() {
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);

        // set the on refresh listener
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // remove old items
                repositoriesAdapter.removeAllItems();
                // load repositories related to the first page
                loadRepositories(1);
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    /**
     * Setting up the recycler view
     */
    private void configRecyclerView() {
        recyclerView = findViewById(R.id.recyclerView);
        // To make scrolling smoothly
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        repositoriesAdapter = new RepositoriesAdapter(this);
        recyclerView.setAdapter(repositoriesAdapter);

        // set the pagination loading
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list

                loadRepositories(page+1);
            }

        };

        recyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);

    }

    /**
     * Loading repositories inside recycler view
     */
    public void loadRepositories(int page){

        // first of all we make sure that the device is connected to internet
        if(!Utility.isNetworkAvailable(this)){
            Toast.makeText(this, R.string.network_unavailable, Toast.LENGTH_SHORT).show();
        }
        else{
            // progress dialog to let the user know that items are loading
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(getString(R.string.loading));
            progressDialog.setTitle(getString(R.string.app_name));
            progressDialog.setCancelable(false);
            progressDialog.show();

            // get the repositories from the repositories controller
            RepositoryController repositoryController = new RepositoryController();
            String date_before_one_month = Utility.getDateBeforeOneMonth(this);
            Call<RepositoryResponse> call_repositories = repositoryController.getRepositories(date_before_one_month, page);

            call_repositories.enqueue(new Callback<RepositoryResponse>() {
                @Override
                public void onResponse(Call<RepositoryResponse> call, Response<RepositoryResponse> response) {
                    Log.i("Repository call", "SUCCESS");

                    RepositoryResponse repositoryResponse = response.body();
                    if(repositoryResponse != null){
                        // get the repositories
                        List<Repository> repositories = repositoryResponse.getItems();
                        // add them to the adapter
                        repositoriesAdapter.addAll(repositories);

                    }

                    // dismiss the progress dialog
                    progressDialog.dismiss();

                }

                @Override
                public void onFailure(Call<RepositoryResponse> call, Throwable t) {
                    Log.w("Repository call", "FAILURE");
                    Toast.makeText(MainActivity.this, R.string.internal_error, Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            });
        }
    }

    /**
     * Show fragment
     */
    public void showFragment(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content_main, fragment, fragment.getClass().getName());
        fragmentTransaction.addToBackStack(fragment.getTag());
        fragmentTransaction.commit();
    }

    /**
     * Hide all fragments
     */
    public void hideFragments(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        for(Fragment fragment : fragments){
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.remove(fragment);
            fragmentTransaction.commit();
        }

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_trending:
                return true;
            case R.id.nav_settings:
                Toast.makeText(MainActivity.this, "Settings", Toast.LENGTH_SHORT).show();
                return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setTitle(R.string.title_activity_main);
        hideFragments();
        navigation.setSelectedItemId(R.id.nav_trending);
    }
}
