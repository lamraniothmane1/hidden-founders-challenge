package hidden_founders.lamrani.com.hiddenfounderscodingchallenge.UI;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import hidden_founders.lamrani.com.hiddenfounderscodingchallenge.R;

public class SplashActivity extends AppCompatActivity {

    public LinearLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // full screen activity
        if(getSupportActionBar() != null){
            getSupportActionBar().hide();
        }
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // get the logo imageview
        layout = findViewById(R.id.splash);

        // animate the image view (fading out)
        animate();

        // start the next activity
        start_next_activity();
    }

    /**
     * Start the new activity
     */
    private void start_next_activity() {
        // this thread will sleep 3 seconds before executing
        Thread timer = new Thread(){
            public void run(){
                try{
                    sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                finally {
                    go_to_next();
                }
            }
        };

        timer.start();
    }

    private void go_to_next() {
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * Animate the logo to fade in when open activity
     */
    private void animate() {
        Animation myAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.splash_transition);
        if(layout != null){
            layout.startAnimation(myAnimation);
        }
    }



}
