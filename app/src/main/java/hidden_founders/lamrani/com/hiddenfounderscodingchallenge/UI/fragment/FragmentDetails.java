package hidden_founders.lamrani.com.hiddenfounderscodingchallenge.UI.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Browser;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import hidden_founders.lamrani.com.hiddenfounderscodingchallenge.API.models.Repository;
import hidden_founders.lamrani.com.hiddenfounderscodingchallenge.Adapters.RepositoriesAdapter;
import hidden_founders.lamrani.com.hiddenfounderscodingchallenge.Helper.Constants;
import hidden_founders.lamrani.com.hiddenfounderscodingchallenge.R;
import hidden_founders.lamrani.com.hiddenfounderscodingchallenge.UI.MainActivity;


/**
 * Created by Lamrani on 10/01/2019.
 */

public class FragmentDetails extends Fragment{

    private View view;
    private transient MainActivity activity;
    private TextView tv_name, tv_fullname, tv_owner, tv_description, tv_rating, tv_licence, tv_url, tv_updated;
    private ImageView iv_avatar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_details, container, false);
        activity = (MainActivity) getActivity();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        // get the repository
        Repository repository = getRepository();

        if(repository == null){
            activity.onBackPressed();
            Toast.makeText(activity, R.string.internal_error, Toast.LENGTH_SHORT).show();
            return;
        }

        // init the view
        initView();

        // load city informations
        loadRepositoryInformation(repository);

    }

    private void initView() {
        tv_name = view.findViewById(R.id.tv_name);
        tv_fullname = view.findViewById(R.id.tv_full_name);
        tv_owner = view.findViewById(R.id.tv_owner);
        tv_description = view.findViewById(R.id.tv_description);
        tv_rating = view.findViewById(R.id.tv_rating);
        tv_licence = view.findViewById(R.id.tv_licence);
        tv_url = view.findViewById(R.id.tv_url);
        tv_updated = view.findViewById(R.id.tv_updated);
        iv_avatar = view.findViewById(R.id.iv_avatar);
    }

    /**
     * Load repository information inside views
     * @param repository
     */
    private void loadRepositoryInformation(final Repository repository) {
        // set name
        tv_name.setText(repository.getName());
        // set full name
        tv_fullname.setText(repository.getFull_name());

        // set owner
        final Repository.Owner owner = repository.getOwner();
        String owner_login = owner != null ? owner.getLogin() : getString(R.string.unavailable);
        tv_owner.setText(owner_login);
        tv_owner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(owner != null){
                    Uri uri = Uri.parse(owner.getHtml_url());
                    Context context = view.getContext();
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    intent.putExtra(Browser.EXTRA_APPLICATION_ID,
                            context.getPackageName());
                    try {
                        context.startActivity(intent);
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        });

        // set URL
        tv_url.setText(repository.getHtml_url());
        tv_url.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View widget) {
                Uri uri = Uri.parse(repository.getHtml_url());
                Context context = widget.getContext();
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.putExtra(Browser.EXTRA_APPLICATION_ID,
                        context.getPackageName());
                try {
                    context.startActivity(intent);
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        // set updated at
        tv_updated.setText(repository.getUpdated_at());

        // set Licence
        Repository.Licence licence = repository.getLicense();
        String licence_name = licence != null ? licence.getName() : getString(R.string.unavailable);
        tv_licence.setText(licence_name);

        // set description
        tv_description.setText(repository.getDescription());

        // set rating
        tv_rating.setText(repository.getStargazers_count());

        // set image with picasso
        String avatar_url = repository.getOwner().getAvatar_url();
        if(avatar_url != null){
            if(!avatar_url.isEmpty()){
                Picasso.get().load(avatar_url)
                        .fit()
                        .error(R.drawable.ic_user)
                        .into(iv_avatar);
            }
        }

    }




    @Override
    public void onResume() {
        super.onResume();
        Repository repository = getRepository();
        if(repository != null){
            activity.setTitle(repository.getName());
        }
        else{
            activity.setTitle(R.string.repository_details);
        }


    }

    public Repository getRepository() {
        return getArguments() != null ? (Repository) getArguments().getSerializable(Constants.REPOSITORY) : null;
    }


}
