package hidden_founders.lamrani.com.hiddenfounderscodingchallenge;

import org.junit.Test;

import java.io.IOException;

import hidden_founders.lamrani.com.hiddenfounderscodingchallenge.API.OperatingApiClient;
import hidden_founders.lamrani.com.hiddenfounderscodingchallenge.API.callbacks.RepositoryAPI;
import hidden_founders.lamrani.com.hiddenfounderscodingchallenge.API.models.RepositoryResponse;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

import static org.junit.Assert.assertTrue;

/**
 * Created by Lamrani on 08/01/2019.
 */

public class RepositoryAPITest {

    @Test
    public void check_api_call() {

        Retrofit retrofit = OperatingApiClient.getClient();
        RepositoryAPI repositoryAPI =  retrofit.create(RepositoryAPI.class);

        Call<RepositoryResponse> call = repositoryAPI.getRepositories();

        try {
            //Magic is here at .execute() instead of .enqueue()
            Response<RepositoryResponse> response = call.execute();
            RepositoryResponse repositoryResponse = response.body();

            assertTrue(response.isSuccessful() && repositoryResponse.getItems() != null);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
